const char *in_memory_pwd = "6213321236";
const int pwd_len = 10;
const int led_pin = 13;

char buf[33];

int check(const char *str1, const char* str2){
  int i=0;
  digitalWrite(led_pin,HIGH);
  while(i < pwd_len && str1[i] == str2[i]) i++;
  digitalWrite(led_pin,LOW);
  if (i < pwd_len)
    return 0;
  else
    return 1;  
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(led_pin,OUTPUT);
  digitalWrite(led_pin,LOW);
}

  

void loop() {
  // put your main code here, to run repeatedly:
  int r;
  
  Serial.setTimeout(10);
  while(!Serial.available())
  delay(1000);
  r = Serial.readBytes(buf,33);
  buf[r]='\0';
  if (check(buf, in_memory_pwd)){
    Serial.println("Good Password");
  }
  else
    Serial.print("Wrong Passwords");
}
