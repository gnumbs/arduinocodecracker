#include <iostream>
#include "rs232.h"
#define PWD_LENGTH 10
#define ARDUINO_SERIAL 0	//Correspond to /dev/ttyACM0 - See rs232.c
#define BDRATE 9600

#ifdef WIN32
#include "../../inc/dwf.h"
#else
#include <digilent/waveforms/dwf.h>
#endif

#ifdef WIN32
    #include <windows.h>
    #define Wait(ts) Sleep(1000*ts)
#else
    #include <unistd.h>
    #include <sys/time.h>
    #define Wait(ts) usleep(1000000*ts)
#endif

using namespace std;

int acquire (const char *);