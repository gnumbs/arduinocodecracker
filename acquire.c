#include "myinclude.h"

int main(int carg, char **szarg){

  char pwd[PWD_LENGTH];
  int i,j,k;
  int one_count=0;
  
  /* Inizialite the serial component. Baud: 9600 */
  char mode[]={'8','N','1',0};
  if(RS232_OpenComport(ARDUINO_SERIAL, BDRATE, mode)) {
    printf("Can not open comport\n");
    return(0);
  }
  
  for (i=0; i<PWD_LENGTH; i++)
    pwd[i]='0';
  pwd[PWD_LENGTH]='\0';
  
  cout << "Cracking process started!" << endl;
  
  for (i=0; i<PWD_LENGTH; i++) {
    
    for (j=0; j<10; j++) {
	
      pwd[i]=j+'0';
      cout << pwd << " - ";
      one_count = acquire(pwd);
      
      if (i==PWD_LENGTH-1) {
	if (one_count >= (i-1)*87+719+17) {
	  cout << "GOT IT!" << endl;
          break;
	} 
      } else {
	  if (one_count >= (719+(i*87)) && one_count < (740+(i*87))) {
	    cout << "GOT IT!" << endl;
	    break;
	  }
      }
      
    }
      
  }
  
  cout << endl << "I GOT THE PASSWORD: " << pwd << endl;
      
}

int acquire (const char * str) {
  
  int i;
  HDWF hdwf;
  STS sts;
  const int cSamples=4096;
  int rgwSamples[cSamples];
  double rgpy[cSamples];
  int samples[cSamples];
  int count=0;
    
  /* This function opens a device identified by the enumeration index and retrieves a handle (hdwf). To automatically 
   * enumerate all connected devices and open the first discovered device, use index -1.
   */
   if(!FDwfDeviceOpen(-1, &hdwf)){
       printf("error");
   }
  
  /* This function is used to set the clock divider value. 
   * Set the sample rate to 100MHz 
   */
  FDwfDigitalInDividerSet(hdwf, 1);
  
  /* This function is used to set the sample format, the number of bits starting from least significant bit. 
   * Valid options are 8, 16, and 32. 
   */
  FDwfDigitalInSampleFormatSet(hdwf, 32);
  
  /* This function is used to set the buffer size. Set to 4096 */
  FDwfDigitalInBufferSizeSet(hdwf, cSamples);
  
  /* This function is used to configure the auto trigger timeout value in seconds. Set to 0. */
  FDwfDigitalInTriggerAutoTimeoutSet(hdwf, 0.0);
  
  
  /* This function is used to set the trigger source for the instrument. */ 
  FDwfDigitalInTriggerSourceSet(hdwf, trigsrcDetectorDigitalIn);
  
  /* This function is used to set the number of samples to acquire after trigger. Set to 4096.  */
  FDwfDigitalInTriggerPositionSet(hdwf, cSamples);
  
  /* This function is used to configure the digital in trigger detector. Set to RisingEdge detector. */
  FDwfDigitalInTriggerSet(hdwf, 0, 0, 1, 0);
  
  /* 1 second of breath. */
  Wait(1);
  
  /* This function is used to configure the instrument and start or stop the acquisition.
   * Here, we start acquisition.
   */
  FDwfDigitalInConfigure(hdwf, false, true);
  
  /* This send on serial port the "str" string. ARDUINO_SERIAL corresponds to /dev/ttyACM0 - See RS232.c for COM ports number. */
  RS232_cputs(ARDUINO_SERIAL, str);
  
  /* Wait the trigger for the acquisition. sts variable, is used to receive the acquisition state. */
  while(sts != stsDone)
    FDwfDigitalInStatus(hdwf, true, &sts);
  
  /* This function is used to retrieve the acquired data samples from the instrument. 
   * It copies the data samples to the provided buffer. 
   */
  FDwfDigitalInStatusData(hdwf, rgwSamples, 4*cSamples);
  
  /* Convert the acquired data and count the number of samples equal to '1' */
  for (i=0; i<cSamples; i++) { 
      rgpy[i]=(double)rgwSamples[i];
      samples[i]=(int)rgpy[i];
      if (samples[i]==1)
	count++;
  }
  
  FDwfDeviceCloseAll();
  cout << "COUNT: " << count << endl;
  return count;  
} 
